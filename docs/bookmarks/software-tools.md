# Software and Tools

## OS
- [Ubuntu 22.04 LTS ](https://ubuntu.com/download/desktop) also called as Jammy JellyFish. Preferred.
- [Windows Subsystem for Linux ](https://docs.microsoft.com/en-us/windows/wsl/) lets developers run a GNU/Linux environment -- including most command-line tools, utilities, and applications -- directly on Windows, unmodified, without the overhead of a traditional virtual machine or dual-boot setup.

## IDE
- [Replit](https://replit.com/) is a simple yet powerful online IDE,
- [Gedit](https://help.gnome.org/users/gedit/stable/) - Default text editor in Ubuntu
- [VSCodium](https://vscodium.com/) is a community-driven, freely-licensed binary distribution of Microsoft’s editor VS Code.
- [PyCharm Community Edition](https://www.jetbrains.com/pycharm/) The Python IDE for Professional Developers
- [KDevelop](https://www.kdevelop.org/) - A cross-platform IDE for C, C++, Python, QML/JavaScript and PHP
- [Geany](https://www.geany.org/) is a powerful, stable and lightweight programmer's text editor that provides tons of useful features without bogging down your workflow. It runs on Linux, Windows and macOS.
- [Sublime Text](https://www.sublimetext.com) is a sophisticated text editor for code, markup and prose. You'll love the slick user interface, extraordinary features and amazing performance.

i#n# Databases
- Redis
- SQLite


## API
- [Insomnia](https://insomnia.rest/) - Open Source API Client can be used to make RESt calls. It also supports importing and editing Open APIs.

## Documentation tools
- [OpenAPI](https://swagger.io/specification/) Specification
- [OpenAPI Online Editor](https://editor.swagger.io/)
- [Draw.io](https://draw.io) Flowchart Maker and Online Diagram Software
